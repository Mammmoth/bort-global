function setTabContent(container, index) {
    $(container)
        .find(".product-item__nav-list .product-item__nav-list-item")
        .eq(index).addClass("active")
        .siblings().removeClass("active");
}

var shredders = function() {
    $(document).foundation();

    //переключение выделения цветом у активного блока меню дропшипинг
    $(".dropshipping-menu-item").on("mouseup", function (e) {
        let s = $(".dropshipping-menu-item-active");
        if (!s.is(e.target) && s.has(e.target).length === 0) {
            s.removeClass("dropshipping-menu-item-active");
        }
    });

    ///lang selector
    $('.shredders-lang-selector__list').click(function () {
        $(this).toggleClass('open');
    });

    $('.shredders-lang-selector__list li').on('click', function () {
        var $list = $('.shredders-lang-selector__list'),
            setLang = $list.data('location'),
            dataLangSelect = $(this).data('lang');
        $list.data('location', dataLangSelect).find('li').removeClass('active');
        $(this).toggleClass('active');
    });

    // products accordeon
    $(".product-item__nav-list-item__title").on("click", function () {
        $(this).parent().toggleClass("active").children().toggleClass("active");
    });


    $('.tabs-wrapper').each(function () {
        var that = $(this);
        var $tab = that.find('.product-item__nav > a');
        $tab.on('click', function (e) {
            var _target = $(e.target),
                activeIndex = _target.index();

            e.preventDefault();
            _target
                .addClass('active')
                .siblings().removeClass('active');
            setTabContent(that, activeIndex);
        });
    });


    // slider
    $(document).ready(function () {
        $('.accessories__slider').owlCarousel({
            items: 1,
            margin: -70,
            loop: true,
            dots: false,
            nav: true,
            navContainer: ".accessories__slider-navigation",
            navText: ["", ""],
            responsive: {
                360: {
                    margin: -70,
                },

                400: {
                    margin: -160,
                },

                530: {
                    items: 2,
                    margin: 0,
                },

                680: {
                    items: 3,
                    margin: 0,
                },

                1150: {
                    items: 4,
                    margin: 40,
                },
            },
        });
    });
}

var tabComponentInit = function () {
    var $tabItems = $('.faq-tabs__nav-side-menu__item'),
        countHidden = $tabItems.not('.hidden').length - 1;

    $('.faq-tabs__more-btn > a').on('click', function (e) {
        e.preventDefault();
        $(this).parent()
            .toggleClass('active')
            .find('span').toggle();
        $tabItems.each(function (idx) {
            if (idx > countHidden) {
                $($tabItems[idx]).toggleClass('hidden');
            }
        });
    });
};

tabComponentInit();
shredders();
